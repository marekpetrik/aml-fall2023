using LinearAlgebra: Symmetric
using Distributions: Normal, Categorical, MultivariateNormal
using DataFrames: DataFrame, nrow, ncol, stack, unstack
using CSV: write, read
using Statistics: mean
using NPZ
#using Impute: filter

# ---------------------------------------------------------
###   Bandit definition
# ---------------------------------------------------------

"""
Represents a linear bandit with some features `Φ` and a parameter vector `θ`.
The return for a action distribution `π` is `π' * Φ' * θ`
"""
struct LinearBandit
    Φ :: Matrix{Float64}
    θ :: Vector{Float64}

    function LinearBandit(Φ::Matrix{Float64} , θ::Vector{Float64})
        length(θ) == size(Φ,1) || error("sizes of θ and Φ do not match")
        new(Φ, θ)
    end
end

"""
Represents a linear bandit with the mean return `μ` and the covariance matrix `Σ`.
The bandit's context is `Φ`. The distribution is over `LinearBandit` and its
paramter `θ`.
"""
struct LinearBanditDistribution
    Φ :: Matrix{Float64}
    μ :: Vector{Float64}
    Σ :: Symmetric{Float64}

    function Bandit(Φ, μ, Σ::Matrix)
        isposdef(Σ) || error("Σ must be positive definite")
        new(Φ, μ, convert(Symmetric{Float64}, Σ))
    end

    function Bandit(Φ, μ, Σ::Symmetric)
        isposdef(Σ) || error("Σ must be positive definite")
        new(Φ, μ, Σ)
    end
end


"""
    generate_linear_bandit(μ₀, Σ₀, Φ)

Generate a `LinearBandit` problem from the mulitvariate normal distribution
with the mean `μ₀` and the covariance matrix `Σ₀`. The bandit's context
is `Φ`.
"""
function generate_linear_bandit(μ₀::Vector{<:Real}, Σ₀::Symmetric{<:Real},
                                Φ::Matrix{<:Real})
    length(μ₀) == size(Σ₀,1) == size(Σ₀,2) || error("dimensions of μ₀ and Σ₀ do not match")
    isposdef(Σ₀) || error("Σ₀ must be positive definite")
    length{μ₀) == size(Φ,2) || error("dimensions of μ₀ and Φ do not match") 
    θ = rand(MultivariateNormal(μ₀, Σ₀), 1) |> vec
    LinearBandit(Φ, θ)
end


"""
    meanreward(θ, a)

Compute the expected reward when taking an action `a` in a bandit `b`.
"""
meanreward(θ, a) = θ[a] 

# ---------------------------------------------------------
###   Generate true bandit data
# ---------------------------------------------------------


"""
    generate_bandits(nbandits, μ₀, Σ₀)

Generate a long datafram of `nbandit` bandit parameter by calling
`generate_bandit`
"""
function generate_bandits(nbandits, μ₀, Σ₀) :: DataFrame
    narms = length(μ₀)
    Bmat = stack((generate_bandit(μ₀, Σ₀) for ib in 1:nbandits), dims=1)
    colnames = map(string, 1:narms)
    B = DataFrame(Bmat, colnames)
    # add the bandit identifier as the first column
    B[!,"bandit"] = 1:nbandits
    # reshape the matrix to a tidy data frame (can be reversed using unstack")
    B = stack(B, colnames, variable_name = "action", value_name = "reward")
    B[!,"action"] = parse.(Int, B[!,"action"])  # reprocess the action identifier
    return B
end


"""
    parse_bandits(B::DataFrame)

Turn a bandit `B::DataFrame` in the long form to a `Matrix`. The function makes
several key of assumptions on the structure of the data.
"""
function parse_bandits(B::DataFrame)
    Bmat = unstack(B, :action, :reward) 
    narms = ncol(Bmat) - 1 # one column is the bandit id
    sort!(Bmat, :bandit)
    # check that the columns and bandit ids are represented correctly
    @assert all(Bmat[:,:bandit] .== 1:nrow(Bmat)) # check bandit assumption
    @assert all(parse.(Int, names(Bmat)[2:end]) .== 1:narms)
    Matrix(Bmat[:, 2:end])
end

# ---------------------------------------------------------
###   Generate rewards
# ---------------------------------------------------------

sample_actions(π, n) = rand(Categorical(π), n)


"""
    sample_rewards(as, θ, σ)

Generate samples of data from the action sequence `as`, the true corresponding bandit
rewards `θ` and the noise `σ`.
"""

function sample_rewards(as::AbstractVector{<:Integer}, θ::AbstractVector{<:Real}, σ::Real)
    n = length(as)
    noise = Normal(0, σ)
    rewards = meanreward.([θ,], as) + rand(noise, n)
    (rewards = rewards)
end          


"""
    sample_rewards(B, π, nrewards)

Generate rewards samples with `nrewards` each for each bandit in the
DataFrame `B`.
"""
function sample_rewards(Bmat::Matrix{<:Real}, π, nrewards)
    nbandits = size(Bmat)[1]
    nobs = nrewards * nbandits  # number of observations (prevent re-allocations)
    bandit = Vector{Int}(undef, nobs)
    arm = Vector{Int}(undef, nobs)
    reward = Vector{Float64}(undef, nobs)
    time = Vector{Int}(undef, nobs)
    
    j = 1
    for (b,θ) in enumerate(eachrow(Bmat))
        as = sample_actions(π, nrewards)
        rews = sample_rewards(as, θ, σ)
        # save data in observations
        for t in eachindex(as, rews)
            bandit[j] = b
            arm[j] = as[t]
            reward[j] = rews[t]
            time[j] = t
            j += 1
        end
    end
    @assert (j-1) == nobs 
    
    DataFrame("bandit"=>bandit, "arm"=>arm, "time"=>time, "reward"=>reward)
end


# ---------------------------------------------------------
###   Evaluate solution
# ---------------------------------------------------------


"""
    policy_mean(bfile, polfile)

Compute the mean return of the policy given in `polfile` for the
bandits given in `bfile`, both CSV files.
"""
function policy_mean(bfile, polfile)
    B = read(bfile, DataFrame)
    S = read(polfile, DataFrame)

    Bmat = parse_bandits(B)
    
    rewards = [Bmat[b,Int(a)] for (b,a) in zip(S[:, :bandit], S[:,:arm])]
    return mean(rewards)
end
