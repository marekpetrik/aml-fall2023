using CSV: read,write
using DataFrames: DataFrame, sort!
using Statistics: mean

include("../bandit.jl") 


function process_results()
    csv_files = [f for f in readdir() if last(split(f,".")) == "csv"]

    students = []
    results = []

    for filename in csv_files
        println("Processing $filename...")
        filename == "bandits.csv" && continue
        student = first(split(filename,"_"))
        meanresult = policy_mean("bandits.csv", filename)
        push!(students, student)
        push!(results, meanresult)
    end
    DataFrame(student=students, result=results)
end


overallresults = process_results()
sort!(overallresults, :result, rev=true);
println(overallresults)




