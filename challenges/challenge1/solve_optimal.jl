using CSV: read, write
using DataFrames: DataFrame, groupby, combine
using Statistics: mean
using LinearAlgebra: diag, diagm


# Important: must match the prior information
narms::Int = 4                                # number of arms for each bandit
nbandits::Int = 100                           # number of offline bandits
μ₀::Vector{Float64} =  zeros(narms)           # prior mean
Σ₀::Matrix{Float64} =  diagm(ones(narms))   # prior covariance matrix
σ::Float64 = 3.                           # standard error of the sampling policy

"""
    solve_bandit_greedy(b, R)

Solves the bandit `b` from  by returning best empirical action.
"""
function solve_bandit_optimal(b, R, narms)
    br = view(R,R.bandit .== b, [:arm, :reward])
    nr = combine(groupby(br, :arm), :reward=>mean, :reward=>length)
    # needed to make sure that the posteriors are ordered in the same way as the priors
    sort!(nr, :arm) 
    # empirical rews
    er = zeros(narms)
    er[nr[:,:arm]] .= nr[:, :reward_mean]    
    # counts
    en = zeros(narms)
    en[nr[:,:arm]] .= nr[:, :reward_length]
    pr = (1.0 ./ (1 ./ diag(Σ₀)  .+ en / σ^2)) .* (μ₀ ./ diag(Σ₀) .+ (er .* en)/σ )
    argmax(pr)
end

function run_solve_bandits()
    R::DataFrame = read("rewards.csv", DataFrame)
    narms = maximum(R[:, :arm])
    bandits::Vector{Int} = unique(R[:, :bandit])
    actions = [solve_bandit_optimal(b, R, narms) for b in bandits]
    DataFrame("bandit"=>bandits, "arm"=>actions)
end

println("Solving ...")
solution::DataFrame = run_solve_bandits()
write("solution.csv", solution)
println("Done.")


