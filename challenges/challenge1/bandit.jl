using Distributions: Normal, Categorical, MultivariateNormal
using DataFrames: DataFrame, nrow, ncol, stack, unstack
using CSV: write, read
using Statistics: mean
#using Impute: filter


# ---------------------------------------------------------
###   Generate true bandit data
# ---------------------------------------------------------

"""
    generate_bandit(μ₀, Σ₀)

Generate bandit problem from the given normal distribution with the multivariate
mean `μ₀` and the covariance matrix `Σ₀`.
"""
generate_bandit(μ₀, Σ₀) = rand(MultivariateNormal(μ₀, Σ₀), 1) |> vec

"""
    generate_bandits(nbandits, μ₀, Σ₀)

Generate a long datafram of `nbandit` bandit parameter by calling
`generate_bandit`
"""
function generate_bandits(nbandits, μ₀, Σ₀) :: DataFrame
    narms = length(μ₀)
    Bmat = stack((generate_bandit(μ₀, Σ₀) for ib in 1:nbandits), dims=1)
    colnames = map(string, 1:narms)
    B = DataFrame(Bmat, colnames)
    # add the bandit identifier as the first column
    B[!,"bandit"] = 1:nbandits
    # reshape the matrix to a tidy data frame (can be reversed using unstack")
    B = stack(B, colnames, variable_name = "action", value_name = "reward")
    B[!,"action"] = parse.(Int, B[!,"action"])  # reprocess the action identifier
    return B
end


"""
    parse_bandits(B::DataFrame)

Turn a bandit `B::DataFrame` in the long form to a `Matrix`. The function makes
several key of assumptions on the structure of the data.
"""
function parse_bandits(B::DataFrame)
    Bmat = unstack(B, :action, :reward) 
    narms = ncol(Bmat) - 1 # one column is the bandit id
    sort!(Bmat, :bandit)
    # check that the columns and bandit ids are represented correctly
    @assert all(Bmat[:,:bandit] .== 1:nrow(Bmat)) # check bandit assumption
    @assert all(parse.(Int, names(Bmat)[2:end]) .== 1:narms)
    Matrix(Bmat[:, 2:end])
end

# ---------------------------------------------------------
###   Generate rewards
# ---------------------------------------------------------

sample_actions(π, n) = rand(Categorical(π), n)

"""
    meanreward(θ, a)

Compute the expected reward when taking an action `a` in a bandit `b`.
"""
meanreward(θ, a) = θ[a] 

"""
    sample_rewards(as, θ, σ)

Generate samples of data from the action sequence `as`, the true corresponding bandit
rewards `θ` and the noise `σ`.
"""
function sample_rewards(as::AbstractVector{<:Integer}, θ::AbstractVector{<:Real}, σ::Real)
    n = length(as)
    noise = Normal(0, σ)
    rewards = meanreward.([θ,], as) + rand(noise, n)
    (rewards = rewards)
end          


"""
    sample_rewards(B, π, nrewards)

Generate rewards samples with `nrewards` each for each bandit in the
DataFrame `B`.
"""
function sample_rewards(Bmat::Matrix{<:Real}, π, nrewards)
    nbandits = size(Bmat)[1]
    nobs = nrewards * nbandits  # number of observations (prevent re-allocations)
    bandit = Vector{Int}(undef, nobs)
    arm = Vector{Int}(undef, nobs)
    reward = Vector{Float64}(undef, nobs)
    time = Vector{Int}(undef, nobs)
    
    j = 1
    for (b,θ) in enumerate(eachrow(Bmat))
        as = sample_actions(π, nrewards)
        rews = sample_rewards(as, θ, σ)
        # save data in observations
        for t in eachindex(as, rews)
            bandit[j] = b
            arm[j] = as[t]
            reward[j] = rews[t]
            time[j] = t
            j += 1
        end
    end
    @assert (j-1) == nobs 
    
    DataFrame("bandit"=>bandit, "arm"=>arm, "time"=>time, "reward"=>reward)
end


# ---------------------------------------------------------
###   Evaluate solution
# ---------------------------------------------------------


"""
    policy_mean(bfile, polfile)

Compute the mean return of the policy given in `polfile` for the
bandits given in `bfile`, both CSV files.
"""
function policy_mean(bfile, polfile)
    B = read(bfile, DataFrame)
    S = read(polfile, DataFrame)

    Bmat = parse_bandits(B)
    
    rewards = [Bmat[b,Int(a)] for (b,a) in zip(S[:, :bandit], S[:,:arm])]
    return mean(rewards)
end
