using CSV: read, write
using DataFrames: DataFrame

"""
    solve_bandit_random(b, R)

Solves the bandit `b` from  by returning the first action
in the reward data `R`.
"""
function solve_bandit_random(b, R)
    first(view(R,R.bandit .== b, :arm)) 
end

function run_solve_bandits()
    R::DataFrame = read("rewards.csv", DataFrame)
    bandits::Vector{Int} = unique(R[:, :bandit])
    actions = [solve_bandit_random(b, R) for b in bandits]
    DataFrame("bandit"=>bandits, "arm"=>actions)
end

println("Solving ...")
solution::DataFrame = run_solve_bandits()
write("solution.csv", solution)
println("Done.")


