using CSV: read,write
using DataFrames: DataFrame, unstack, sort, nrow
using Statistics: mean


include("bandit.jl")

meanresult = policy_mean("bandits.csv", "solution.csv")

print("Mean reward attained is ", meanresult)



