using LinearAlgebra: diagm

include("bandit.jl")

narms::Int = 4                                # number of arms for each bandit
nbandits::Int = 500                           # number of offline bandits
μ₀::Vector{Float64} =  zeros(narms)           # prior mean
Σ₀::Matrix{Float64} =  diagm(ones(narms))   # prior covariance matrix

#[ 4. 2 0 -1;
#  2  3 1  0;
#  0  1 2  0;
# -1  0 0  1]

println("Generating bandits ... ")
B = generate_bandits(nbandits, μ₀, Σ₀)
write("bandits.csv", B)
