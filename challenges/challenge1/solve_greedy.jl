using CSV: read, write
using DataFrames: DataFrame, groupby, combine
using Statistics: mean

"""
    solve_bandit_greedy(b, R)

Solves the bandit `b` from  by returning best empirical action.
"""
function solve_bandit_greedy(b, R, narm)
    br = view(R,R.bandit .== b, [:arm, :reward])
    nr = combine(groupby(br, :arm), :reward=>mean)
    sort!(nr, :arm)
    er = zeros(narm)
    er[nr[:,:arm]] .= nr[:, :reward_mean]    
    argmax(er)
end

function run_solve_bandits()
    R::DataFrame = read("rewards.csv", DataFrame)
    narms = maximum(R[:, :arm])
    bandits::Vector{Int} = unique(R[:, :bandit])
    actions = [solve_bandit_greedy(b, R, narms) for b in bandits]
    DataFrame("bandit"=>bandits, "arm"=>actions)
end

println("Solving ...")
solution::DataFrame = run_solve_bandits()
write("solution.csv", solution)
println("Done.")


