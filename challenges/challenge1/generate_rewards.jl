include("bandit.jl")

B::DataFrame = read("bandits.csv", DataFrame)
Bmat::Matrix{Float64} = parse_bandits(B)
narms = size(Bmat)[2]

nrewards::Int = 20                        # number of rewards observed for each bandit
π::Vector{Float64} = ones(narms) / narms  # sampling policy  
σ::Float64 = 3.                           # standard error of the sampling policy
    
println("Generating rewards ... ")
R = sample_rewards(Bmat, π, nrewards)
write("rewards.csv", R)
println("Done.")
