using LinearAlgebra: diagm

include("bandit.jl")

nbandits::Int = 500                           # number of offline bandits

#[ 4. 2 0 -1;
#  2  3 1  0;
#  0  1 2  0;
# -1  0 0  1]

println("Generating bandits ... ")
bandits::Bandits = make_bandits(nbandits, prior)
df = dataframe(bandits)


# make sure that the bandits are recovered correctly
bandits2::Bandits = parse(df, Bandit)
@assert maximum(distance.(bandits, bandits2)) ≤ 1e-4  

write("bandits.csv", df)
println("Done.")
