\documentclass[11pt,oneside,letterpaper]{article}

\usepackage{fullpage}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{bm}

\newcommand{\Real}{\mathbb{R}}
\newcommand{\tr}{^{\top}}
\newcommand{\opt}{^{\star}}
\newcommand{\E}[1]{\mathbb{E}\left[#1\right]}
\renewcommand{\P}[1]{\mathbb{P}\left[#1\right]}

\setlength{\parskip}{2mm}
\setlength{\parindent}{0pt}

\title{Challenge 2: High-confidence Guarantees in Bandits}
\author{Marek Petrik}

\begin{document}

\maketitle

\section{Description}

Consider the following situation. You ran a clinical trial with a large number of patients. Each of the patients has a chronic condition that requires continuous treatment, such as high blood pressure. After each one of the medications was administered, the patient's health was monitored and the response was recorded. Your goal is to design a treatment protocol based on the data from the clinical trial. It is important that the treatment protocol that you design is conservative and focuses on helping as many patients as possible, rather than just performing well on average.

We can formalize this setting as a multi-armed bandit problem. There are $k$ arms and $m$ possible bandits. Each arm represents a treatment and each bandit represents a patient (or a group of patients). For any bandit $l$, let $\bm{\mu}^l \in \Real^k$ be the (unknown) mean of returns of the arms $1, \dots, k$. For each bandit $l \in [m]$, we will seek to compute a policy (or a treatment protocol) $\bm{\pi}\in \Delta_k$ that maximizes the \emph{average} return:
\begin{equation}\label{eq:ideal}
 \max_{\bm{\pi}\in \Delta_k} \bm{\pi}\tr \bm{\mu}^{l}.
\end{equation}
Here, $\Delta_k$ is the set of $k$-dimensional probability distributions. In other words, we allow the treatment protocols to randomize. The rationale for randomization is that if you are not sure which treatment is preferable to a patient, it may be better to assign one treatment sometimes and the other one at other times.

The notation used is as follows. Bold font denotes vectors (lower case) and matrices (upper case). For example, $\mu_1^2$ represents the mean value of arm 1 in bandit 2. Random variables are denoted by the tilde.

Since $\bm{\mu}^l$ is not known in \eqref{eq:ideal} it cannot be solved and we need some other objectives and algorithms. 

The clinical trial data is of the form $(a_1, r_1, a_2, r_2, \dots )$ with $a_t \in [m]$ representing the arms (actions) taken and $r_t\in \Real$ representing the rewards received. The rewards are distributed as $r_t \sim \mathcal{N}(\mu_{a_t}, \sigma^2)$ with a known $\sigma^2$. Given the clinical trial data $\mathcal{D}$ and some known (normal) prior distribution over $\tilde{\bm{\mu}}$, we can compute the posterior distribution, which we denote with the following shorthand: $\tilde{\bm{\mu}} \mid \mathcal{D}$. 

In the first challenge, we considered the same setting and optimized for the Bayesian objective:
\[
 \max_{\bm{\pi}\in \Delta_{k}}  \E{ \bm{\pi}\tr \bm{\tilde{\mu}}^l \mid  \mathcal{D}}
\]
for each bandit $l\in [m]$ individually. That is the goal was to maximize the \emph{expected} rewards, or the rewards for the average model. This kind of objective is often undesirable because it may result in some patients having catastrophic outcomes. 

One can also think of the bandit (or patient) $l$ as a random variable $\tilde{l}$. The patients $l_1, \dots, l_m$ in the clinical trial can then be seen as samples from the random variable $\tilde{l}$. With this setup, an example of an objective that minimizes the downside risk of the patients is for any $\alpha\in (0,1)$:
\[
  \max_{\bm{\pi}\in \Delta_{k}} \max
  \left\{ \zeta \in  \Real  \mid
           \P{\bm{\pi}\tr \bm{\tilde{\mu}}^{\tilde{l}} \ge \zeta \mid  \mathcal{D}} \ge 1-\alpha \right\} .
\]
To make this more concrete, consider $\alpha = 0.05$. If you solve the optimization problem above, and the optimal value is $\zeta\opt = 10$, then at least $95\%$ of patients (bandits) will achieve a reward of at least $10$ or more when following the optimal policy $\bm{\pi}\opt$.

The assignment is as follows:
\begin{enumerate}
\item Can you think of other objectives that would be appropriate in this setting? Possible options include objectives stated in terms of frequentist as well as Bayesian guarantees. Also one may consider stating these guarantees both in terms of the absolute rewards as well as in terms of regrets.
\item How would you go about evaluating the objectives that you propose numerically? 
\item Can you think of what may be some algorithms appropriate for solving the objective that you propose?
\end{enumerate}

Please try to be as specific and formal as you can be, but any ideas that you may have will be helpful. Note that frequentist goals can be difficult to formalize as optimization problems, but try to at least think how one may go about describing of evaluating the objective. 

\section{Solutions}

You know: $\alpha, \mathcal{D}, \sigma, \bm{\mu}_0, \bm{\Sigma}_0$

\paragraph{1.1 Fixed policy, overall guarantee}

\[
  \max_{\bm{\pi}\in \Delta_{k}} \max
  \left\{ \zeta \in  \Real  \mid
           \P{\bm{\pi}\tr \bm{\tilde{\mu}}^{\tilde{l}} \ge \zeta \mid  \mathcal{D}} \ge 1-\alpha \right\} .
\]

\paragraph{1.2 Contextual policy, overall guarantee}

\[
  \max_{\bm{\pi}_l \in \Delta_{k}, l = 1, \dots , m} \max
  \left\{ \zeta \in  \Real  \mid
           \P{\bm{\pi}_{\tilde{l}}\tr \bm{\tilde{\mu}}^{\tilde{l}} \ge \zeta \mid  \mathcal{D}} \ge 1-\alpha \right\} .
\]


\paragraph{1.3 Contextual policy, group guarantee}

\begin{gather*}
\max_{\bm{\pi}_l \in \Delta_{k}, l = 1, \dots , m} 
  \frac{1}{m} \sum_{l=1}^m \max
  \left\{ \zeta \in  \Real  \mid
             \P{\bm{\pi}_{l}\tr \bm{\tilde{\mu}}^{l} \ge \zeta \mid  \mathcal{D}} \ge 1-\alpha \right\} 
         =\\
  \frac{1}{m} \sum_{l=1}^m 
\max_{\bm{\pi} \in \Delta_{k}} \max
  \left\{ \zeta \in  \Real  \mid
             \P{\bm{\pi}\tr \bm{\tilde{\mu}}^{l} \ge \zeta \mid  \mathcal{D}} \ge 1-\alpha \right\} .
\end{gather*}



\paragraph{2.1 Fixed policy, overall regret guarantee}

\[
  \min_{\bm{\pi}\in \Delta_{k}} \min
  \left\{ \zeta \in \Real  \mid
           \P{\tilde{\mu}_{\tilde{a}\opt}^{\tilde{l}} - \bm{\pi}\tr \bm{\tilde{\mu}}^{\tilde{l}} \le \zeta \mid  \mathcal{D}} \ge 1-\alpha \right\} .
\]

\paragraph{2.2 Contextual policy, overall regret guarantee}

\[
  \min_{\bm{\pi}_l \in \Delta_{k}, l = 1, \dots , m} \min
  \left\{ \zeta \in  \Real  \mid
           \P{\tilde{\mu}_{\tilde{a}\opt}^{\tilde{l}} - \bm{\pi}_{\tilde{l}}\tr \bm{\tilde{\mu}}^{\tilde{l}} \le \zeta \mid  \mathcal{D}} \ge 1-\alpha \right\} .
\]


\paragraph{2.3 Contextual policy, group regret guarantee}

\begin{gather*}
\min_{\bm{\pi}_l \in \Delta_{k}, l = 1, \dots , m} 
  \frac{1}{m} \sum_{l=1}^m \min
  \left\{ \zeta \in  \Real  \mid
    \P{\tilde{\mu}_{\tilde{a}\opt}^{\tilde{l}} -
      \bm{\pi}_l\tr \bm{\tilde{\mu}}^{l} \le \zeta \mid  \mathcal{D}} \ge 1-\alpha \right\} 
         =\\
  \frac{1}{m} \sum_{l=1}^m 
\min_{\bm{\pi} \in \Delta_{k}} \min
  \left\{ \zeta \in  \Real  \mid
    \P{\tilde{\mu}_{\tilde{a}\opt}^{\tilde{l}} - \bm{\pi}\tr \bm{\tilde{\mu}}^{l} \le \zeta \mid  \mathcal{D}} \ge 1-\alpha \right\} .
\end{gather*}

\paragraph{3.1 Frequentist Fixed policy, overall guarantee}

We want to compute a policy $\bm{\pi}\colon D \to \Delta_k$ such that we have the following guarantee for each $\bm{\mu}\opt$
\[
  \max
  \left\{ \zeta \in  \Real  \mid
           \P{\bm{\pi}(\tilde{D})\tr (\bm{\mu}\opt)^{\tilde{l}} \ge \zeta } \ge 1-\alpha \right\} .
\]

\end{document}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
