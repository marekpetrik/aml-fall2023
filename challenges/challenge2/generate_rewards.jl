include("bandit.jl")


nrewards::Int = 5                        # number of rewards observed for each bandit
    
function run_generate_rewards()

    # sampling policy
    π = ones(narms) / narms  # sampling policy  
    println("Generating rewards ... ")

    bandits_df = read("bandits.csv", DataFrame)
    bandits = parse(bandits_df, Bandit)

    # assume all bandits have the same number of arms
    @assert all(narms .== map(b->length(b.θ), bandits))

    actions = [make_actions(π, nrewards) for i ∈ 1:length(bandits)]
    sims = simulate.(bandits, σ_reward, actions)
    sims_df = dataframe(sims)

    # test that we can recover the simulations
    sims2 = parse(sims_df, Simulation)
    @assert maximum(distance.(sims, sims2)) ≤ 1e-4

    write("rewards.csv", sims_df)
    println("Done.")
end

run_generate_rewards()
