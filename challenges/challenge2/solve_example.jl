using CSV: read, write
using DataFrames: DataFrame, groupby, combine
using Statistics: mean

include("bandit.jl")

#### ----------------------------------
# Solvers
# -------------------------



"""
    solve_greedy(sim, nactions)

Pick the best empirical arm from the simulation `sim` which
has at least one observation.
"""
function solve_greedy(sim::Simulation,  nactions::Integer)
    ags = aggregate(sim, nactions)
    # set unobserved ones to be -Inf
    x = ags.means  
    x[ags.counts .== 0] .= -Inf
    π = zeros(nactions)
    π[argmax(x)] = 1.0
    return Policy(sim.banditindex, π)
end


"""
    solve_optimal_mean(sim, nactions)

Pick the best empirical arm from the simulation `sim` which
has at least one observation.
"""
function solve_optimal_mean(prior::BanditDistribution, σ::Real,
                      sim::Simulation, nactions::Integer)
    μ_post = infer_posterior_mean(prior, sim, nactions, σ)
    π = zeros(nactions)
    π[argmax(μ_post)] = 1.0
    return Policy(sim.banditindex, π)
end



"""
    solve_chernoff_det(prior, σ, sim, nactions)

Solve for the optimal high-confidence action using the Chernoff bound,
using the deterministic action.
"""
function solve_chernoff_det(prior::BanditDistribution, σ::Real,
                            sim::Simulation, nactions::Integer,
                            α::Real)

    posterior = infer_posterior(prior, sim, σ)
    
    means = posterior.μ₀
    variances = diag(posterior.Σ₀)
    ζ = √(2*log(1/α))
    amax = argmax(means .+ .√(variances) .* ζ)
    π = zeros(nactions)
    π[amax] = 1
    return Policy(sim.banditindex, π)
end


"""
    solve_optimal_det(prior, σ, sim, nactions)

Solve for the optimal high-confidence action using the Chernoff bound,
using the deterministic action.
"""
function solve_optimal_det(prior::BanditDistribution, σ::Real,
                           sim::Simulation, nactions::Integer,
                           α::Real)

    posterior = infer_posterior(prior, sim, σ)
    snd = Normal(0,1)
    
    means = posterior.μ₀
    variances = diag(posterior.Σ₀)
    amax = argmax(means .+ .√(variances) .* quantile(snd, α))
    π = zeros(nactions)
    π[amax] = 1
    return Policy(sim.banditindex, π)
end

#### ----------------------------------
# Solution methods
# -------------------------

function solve(algorithm::Function)
    rewards_df = read("rewards.csv", DataFrame)
    sims = parse(rewards_df, Simulation)
    nactions = mapreduce(s->maximum(s.actions), max, sims; init=-1)
    algorithm.(sims, nactions)
end

#### ----------------------------------
# Script
# -------------------------

println("Solving greedy ...")
policies = solve(solve_greedy)
policies_df = dataframe(policies)
policies2 = parse(policies_df, Policy)
@assert maximum(distance.(policies, policies2)) ≤ 1e-4
write("solution_marek_greedy.csv", policies_df)

println("Solving optimal ...")
policies = solve( (sim, nactions) -> solve_optimal_mean(prior, σ_reward, sim, nactions))
write("solution_marek_optimal_mean.csv", dataframe(policies))

α::Float64 = 0.05 

println("Solving Chernoff ...")
policies = solve( (sim, nactions) -> solve_chernoff_det(prior, σ_reward, sim, nactions, α))
write("solution_marek_chernoff_conf.csv", dataframe(policies))

println("Solving Chernoff ...")
policies = solve( (sim, nactions) -> solve_optimal_det(prior, σ_reward, sim, nactions, α))
write("solution_marek_optimal_conf.csv", dataframe(policies))

println("Done.")
