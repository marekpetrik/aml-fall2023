using CSV: read, write
using DataFrames: DataFrame, sort!
using Statistics: mean

include("bandit.jl") 

function process_results(dirname = pwd())
    csv_files = [f for f in readdir(dirname) if last(split(f,".")) == "csv"]

    output = (
        students = Vector{String}(),
        algos = Vector{String}(),
        results = Vector{Float64}())

    bandits = read(joinpath(dirname, "bandits.csv"), DataFrame) |> (x->parse(x,Bandit))
    
    for filename in csv_files
        println("Processing $filename...")
        if !startswith(filename, "solution_")
            println("    not a solution, skipping.")
            continue
        end
        nameparts = split(filename,"_")
        if length(nameparts) < 3
            println("    missing student or algorithm, skipping.")
            continue
        end
        student = nameparts[2]
        algorithm = nameparts[3]

        policies = read(joinpath(dirname, filename), DataFrame) |> (x->parse(x,Policy))
        meanresult = policy_mean(bandits, policies)
        
        push!(output.students, student)
        push!(output.algos, algorithm)
        push!(output.results, meanresult)
    end
    DataFrame(student=output.students, algorithm=output.algos, result=output.results)
end


overallresults = process_results()
sort!(overallresults, :result, rev=true);
println(overallresults)




