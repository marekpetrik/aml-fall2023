using Distributions: Normal, Categorical, MultivariateNormal, quantile
using DataFrames: DataFrame, nrow, ncol, stack, unstack
using CSV: write, read
using Statistics: mean
using Tables: columntable
using LinearAlgebra: Symmetric, isposdef, norm, diag, diagm, isdiag
import Base: length, resize!
#using Impute: filter



# ---------------------------------------------------------
###   Bandit definition
# ---------------------------------------------------------


"""
    BanditDistribution(μ₀, Σ₀)

Prior normal distribution over the bandits. This is also the environment
that describes the bandits, as used in the BanditAlgorithms book. 
"""
struct BanditDistribution
    μ₀::Vector{<:Real}
    Σ₀::Symmetric{<:Real}

    function BanditDistribution(μ₀, Σ₀)
        length(μ₀) == size(Σ₀,1) || error("dimensions of μ₀ and Σ₀ do not match")
        isposdef(Σ₀) || error("Σ₀ must be positive definite")
        new(μ₀, Σ₀)
    end
end



"""
    Bandit(index, θ)

Represents a bandit with a parameter vector `θ`. The return for an action
distribution `π` is `π' * θ`
"""
struct Bandit
    index :: Int
    θ :: Vector{Float64}
end

distance(b1::Bandit, b2::Bandit) = norm(b1.θ .- b2.θ)

const Bandits = Vector{Bandit}

"""
    Policy(banditindex, π)

A policy which represents the solution to a bandit problem
"""
struct Policy
    banditindex :: Int
    π :: Vector

    function Policy(banditindex::Int, π::Vector{Float64})
        minimum(π) ≥ 0 || error("policy must be non-negative")
        new(banditindex, π ./ sum(π))
    end
end

const Policies = Vector{Policy}

distance(p1, p2) = 100 * abs(p1.banditindex - p2.banditindex) +
    norm(p1.π .- p2.π)


"""
    meanreward(θ, policy)

Compute the expected reward when taking action distribution `π` in a bandit `b`.
If `π` is an integer, it is interpreted as a deterministic action, otherwise
it is a distribution over the actions. 
"""
function meanreward(b::Bandit, p::Policy)
    b.index == p.banditindex || error("bandit and policy indices do not match")
    return b.θ' * p.π
end

meanreward(b::Bandit, π::AbstractVector) = meanreward(b, Policy(b.index, π))
meanreward(b::Bandit, a::Integer) = b.θ[a] 


"""
    varreward(posterior, policy, α)

Computes VaR of the rewards for the posterior distribution over the bandits
with risk level `α`. Note that this evaluation metric needs not to know
the actual bandit rewards.
"""
function varreward(posterior::BanditDistribution, p::Policy, α::Real)
    snd = Normal(0,1)
    mean = posterior.μ₀' * p.π
    variance = p.π' * posterior.Σ₀ * p.π
    return mean + √(variance) * quantile(snd, α)
end

# ---------------------------------------------------------
###   Generate true bandit data
# ---------------------------------------------------------

"""
    make_bandit(index, prior)

Generate a `Bandit` problem with an `index` from the `prior` distribution 
"""
function make_bandit(index, prior::BanditDistribution) :: Bandit
    θ = rand(MultivariateNormal(prior.μ₀, prior.Σ₀), 1) |> vec
    Bandit(index, θ)
end

"""
    make_bandits(nbandits, μ₀, Σ₀)

Generate `nbandit` of `Bandit` problems from the mulitvariate normal distribution
with the mean `μ₀` and the covariance matrix `Σ₀`. 
"""
function make_bandits(nbandits::Integer, prior::BanditDistribution) :: AbstractArray{Bandit}
    [make_bandit(i, prior) for i ∈ range(1,nbandits)]
end


# ---------------------------------------------------------
###   Bandit processing 
# ---------------------------------------------------------
     
"""
    dataframe(bandits)

Convert a list of bandits to a dataframe
"""
function dataframe(bandits::AbstractVector{Bandit}) :: DataFrame
    output = (bandit = Vector{Int}(), arm = Vector{Int}(), reward = Vector{Float64}())

    allunique(getproperty.(bandits, :index)) || error("bandit indexes must be unique")

    for b ∈ bandits
        for a ∈ eachindex(b.θ)
            push!(output.bandit, b.index)
            push!(output.arm, a)
            push!(output.reward, b.θ[a])
        end
    end
    DataFrame(output)
end

"""
    parse(df::DataFrame, ::Type{Bandit})

Turn a bandit `df::DataFrame` into a vector of bandits

Assumes that all bandits are sequential and have the same number of arms
"""
function parse(df::DataFrame, ::Type{Bandit}) :: Vector{Bandit}
    @assert all(names(df) .== ("bandit", "arm", "reward"))
    bandits = (bandit = df.bandit :: AbstractVector{Int},
               arm = df.arm :: AbstractVector{Int},
               reward = df.reward :: AbstractVector{Float64})
    # make sure that the indices make sense
    @assert minimum(bandits.bandit) == 1 && minimum(bandits.arm) == 1 

    narms = maximum(bandits.arm)
    output = [Bandit(bi, fill(NaN, narms)) for bi ∈ 1:maximum(bandits.bandit)]
    for i ∈ eachindex(bandits...)
        output[bandits.bandit[i]].θ[bandits.arm[i]] = bandits.reward[i]
    end
    return output
end



#### -------------------------------------------------------
#   Policy processing
# ---------------------------------------------------------

"""
    dataframe(policies)

Turns a list of randomized policies to a dataframe with policies.
Only works when bandit policies are all for different bandits.
"""
function dataframe(policies::Policies)
    @assert !isempty(policies)
    nactions = length(first(policies).π)
    npol = length(policies)
    
    @assert all(map(p->length(p.π), policies) .== nactions)
    @assert Set(getfield.(policies, :banditindex)) == Set(1:npol)
    
    output = (bandit = Vector{Int}(),
              arm = Vector{Int}(),
              probability = Vector{Float64}())

    for p ∈ policies
        for i ∈ eachindex(p.π)
            push!(output.bandit, p.banditindex)
            push!(output.arm, i)
            push!(output.probability, p.π[i])
        end
    end
    return DataFrame(output)
end

"""
    dataframe(policies)

Turns a list of randomized policies to a dataframe with policies. Only works when bandit
policies are all for different bandits.
"""
function parse(df::DataFrame, ::Type{Policy}) :: Policies
    input = (bandit = df.bandit :: AbstractArray{Int},
             arm = df.arm :: AbstractArray{Int},
             probability = df.probability :: AbstractArray{Float64})

    @assert minimum(input.bandit) ≥ 1 && minimum(input.arm) ≥ 1
    narms = maximum(input.arm)
    nbandits = maximum(input.bandit)

    πs = fill(NaN, (nbandits, narms))
    
    sims = [Simulation(i, Vector{Int}(), Vector{Float64}()) for i ∈ 1:nbandits]
    for i ∈ eachindex(input...)
       πs[input.bandit[i], input.arm[i]] = input.probability[i]
    end
    # construct the policies
    # TODO: better to use rows or columns for cache?
    return [Policy(i, collect(a)) for (i,a) ∈ enumerate(eachrow(πs))]
end



#### -------------------------------------------------------
#   Define rewards
# ---------------------------------------------------------

"""
    Simulation(banditindex, actions, rewards)

A result of a simulation of a bandit with `banditindex`. In particular,
it contains the `rewards` attained as a function of `actions` taken.
"""
struct Simulation
    banditindex :: Int
    actions :: Vector{Int}
    rewards :: Vector{Float64}

    function Simulation(banditindex::Int, actions::Vector{Int}, rewards::Vector{Float64})
        length(actions) == length(rewards) || error("action and reward length must match.")
        new(banditindex, actions, rewards)
    end
end

const Simulations = Vector{Simulation}

# used to check equivalence
distance(sim1::Simulation, sim2::Simulation) =
    max(100 * abs(sim1.banditindex - sim2.banditindex),
        norm(sim1.actions .- sim2.actions),
        norm(sim1.rewards .- sim2.rewards))

"""
    simulate(b, σ, actions)

Generate samples of data from the action sequence `actions`, the bandit `b` and the noise `σ`.
"""
function simulate(b::Bandit, σ::Real, actions::AbstractVector{<:Integer})
    noise = Normal(0, σ)
    rewards = meanreward.((b,), actions) .+ rand(noise, length(actions))
    Simulation(b.index, actions, rewards)
end

length(s::Simulation) = length(s.actions)

"""
    resize!(sim, n)

Resize `sim` to contain `n` elements. If `n` is smaller than the current collection length,
the first n elements will be retained. If `n` is larger, the new elements are initialized
to `NaN`.
"""
function resize!(sim::Simulation, n::Integer)
    lastsize = length(sim)
    resize!(sim.actions, n)
    resize!(sim.rewards, n)
    if n > lastsize
        sim.actions[lastsize+1:n] .= -1
        sim.rewards[lastsize+1:n] .= NaN
    end
end


# ---------------------------------------------------------
###   Parse rewards
# ---------------------------------------------------------

make_actions(π::AbstractVector{<:Real}, n::Integer) :: Vector{Int} =
    rand(Categorical(π), n)

"""
    dataframe(sims)

Converts simulations `sims` for bandits to a data frame
"""
function dataframe(sims::Vector{Simulation})
    nobs = mapreduce(s->length(s), +, sims)
    output = (bandit = Vector{Int}(undef, nobs),
              arm = Vector{Int}(undef, nobs),
              time = Vector{Int}(undef, nobs),
              reward = Vector{Float64}(undef, nobs))

    allunique(getfield.(sims, :banditindex)) || error("one sim per bandit")

    j = 1
    for sim ∈ sims
        for t in eachindex(sim.actions, sim.rewards)
            output.bandit[j] = sim.banditindex
            output.arm[j] = sim.actions[t]
            output.reward[j] = sim.rewards[t]
            output.time[j] = t
            j += 1
        end
    end
    @assert (j-1) == nobs 
    DataFrame(output)
end


"""
    parse(df, ::Type{Simulation})

Loads a set of simulations from a dataframe. Assumes a single simulation per bandit.
The method should also work when the number of simulations per bandit is not
constant.
"""
function parse(df::DataFrame, ::Type{Simulation}) :: Simulations
    @assert all(names(df) .== ["bandit", "arm", "time", "reward"])
    input = (bandit = df.bandit :: AbstractVector{Int},
             arm = df.arm :: AbstractVector{Int},
             time = df.time :: AbstractVector{Int},
             reward = df.reward :: AbstractVector{Float64})

    @assert minimum(input.bandit) ≥ 1 && minimum(input.arm) ≥ 1 &&
        minimum(input.time) ≥ 1

    nbandits = maximum(input.bandit)
    sims = [Simulation(i, Vector{Int}(), Vector{Float64}()) for i ∈ 1:nbandits]
    for i ∈ eachindex(input...)
        s = sims[input.bandit[i]]
        t = input.time[i]
        # increase the size if needed
        (length(s) < t) &&  resize!(s, t)
        s.actions[t] = input.arm[i]
        s.rewards[t] = input.reward[i]
    end
    return sims
end


### -------------------------------------------------------
#     Bayesian and frequentist inference
# ---------------------------------------------------------

"""
    aggregate(sim, nactions)

Aggregate the simulation data from `sim` and return useful
statistics, such as means and counts. The number of arms/actions
is `nactions`
"""
function aggregate(sim::Simulation, nactions::Int)
    # set unobserved ones to be -Inf
    means = fill(0e0, nactions)
    counts = fill(0, nactions)
    for (a,r) ∈ zip(sim.actions, sim.rewards)
        means[a] += r
        counts[a] += 1
    end
    return (means=means, counts=counts)
end

"""
    infer_posterior_mean(prior, sim, nactions, σ)

Compute the posterior means for a given `prior`, `sim` simulation
data, and the noise `σ`. The number of arms/actions is `nactions`
"""
function infer_posterior_mean(prior::BanditDistribution, sim::Simulation,
                              nactions::Int, σ::Real) :: Vector{Float64}
    # TODO: nactions is superfluous in this setting since we have the prior
    isdiag(prior.Σ₀) || error("Non-diagonal priors not supported yet")
    stats = aggregate(sim, nactions)
    return (1.0 ./ (1.0 ./ diag(prior.Σ₀)  .+ stats.counts / σ^2)) .*
        (prior.μ₀ ./ diag(prior.Σ₀) .+ (stats.means .* stats.counts) ./ (σ^2))
end


"""
    infer_posterior_var(prior, sim, nactions, σ)

Compute the posterior variance for a given `prior`, `sim` simulation
data, and the noise `σ`. The number of arms/actions is `nactions`
"""
function infer_posterior_var(prior::BanditDistribution, sim::Simulation,
                              nactions::Int, σ::Real) :: Vector{Float64}
    isdiag(prior.Σ₀) || error("Non-diagonal priors not supported yet")
    stats = aggregate(sim, nactions)
    return (1. ./ diag(prior.Σ₀) .+ stats.counts ./ (σ^2))
end

"""
    infer_posterior(prior, sim, σ)

Compute the posterior mean and variance for a given `prior`, `sim` simulation
data, and the noise `σ`. The number of arms/actions is `nactions`
"""
function infer_posterior(prior::BanditDistribution, sim::Simulation,
                         σ::Real) :: BanditDistribution

    nactions = length(prior.μ₀)
    isdiag(prior.Σ₀) || error("Non-diagonal priors not supported yet")
    stats = aggregate(sim, nactions)
    μ = (1.0 ./ (1.0 ./ diag(prior.Σ₀)  .+ stats.counts / σ^2)) .*
        (prior.μ₀ ./ diag(prior.Σ₀) .+ (stats.means .* stats.counts) ./ (σ^2))
    Σ = diagm(1. ./ diag(prior.Σ₀) .+ stats.counts ./ (σ^2))
    BanditDistribution(μ, Symmetric(Σ))
end

### -------------------------------------------------------
#     Evaluate solution
# ---------------------------------------------------------


"""
    policy_mean(bandits, policies)

Evaluate the mean return of `policies` on the corresponding `bandits`
"""
function policy_mean(bandits, policies)
    length(bandits) == length(policies) ||
        error("numbers of bandits and policies must match.")
    mean(meanreward.(bandits, policies))
end

"""
    policy_hc_each(sims, policies, α)

Evaluate the bandits' high-condifence return of `policies`
for `sims` with normally distributed arms with tolerance `α`. The confidence
is computed with respect to each posterior separately and `α=0` is
the highest confidence level. Still computes a mean over all the bandits.
"""
function policy_hc_each(sims::Simulations, σ::Real, policies::Policies, α::Real)
    length(sims) == length(policies) ||
        error("numbers of simulations and policies must match.")
    posteriors = infer_posterior.((prior,), sims, σ)
    mean(varreward.(posteriors, policies, α))
end


### -------------------------------------------------------
#     Constants
# ---------------------------------------------------------

const narms = 10
const prior = BanditDistribution(collect(1:narms), Symmetric(diagm(collect(1:narms))))
const σ_reward = 10.
