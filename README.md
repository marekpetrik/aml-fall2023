# CS 950: Advanced Machine Learning (Fall 2023)

This class will cover topics at the intersection of machine learning and decision theory. The main focus of the class will be on developing theoretical depth in machine learning and decision making along with the implementation of simple corresponding algorithms. 

### When and Where 

*Lectures*: MW, 10:10 am - 11:30 am

*Where*: Kingsbury N233

## Schedule (Subject to change)

We will adjust class content depending on preferences and progress with the material. Empty slots are TBD based on the class discussion. 

| Date   | Day | Topic                       | Reading       | Class lead       |
|--------|-----|-----------------------------|---------------|------------------|
| Aug 28 | Mon | Introduction to bandits     | BA 1          | Marek            |
| Aug 30 | Wed | Probability                 | BA 2          | Marek            |
| Sep 4  | Mon | *No class*                  |               |                  |
| Sep 6  | Wed | Challenge 1 (bandits)       |               | Marek            |
| Sep 11 | Mon | Stochastic bandits          | BA 4          | Gersi            |
| Sep 13 | Wed | Concentrations              | BA 5          | Monkie           |
| Sep 18 | Mon | Explore then commit         | BA 6          | Xihong           |
| Sep 20 | Wed | Upper confidence bound      | BA 7,8        | Bryan            |
| Sep 25 | Mon | Upper confidence bound 2    | BA 7,8        | Bryan            |
| Sep 27 | Wed | Challenge 2 (confidence)    | ??            | Marek            |
| Oct 2  | Mon | Challenge 2.1 (confidence)  |               | Marek            |
| Oct 4  | Wed | Adversarial bandits         | BA 11         | Saber            |
| Oct 9  | Mon | *No class*                  |               |                  |
| Oct 11 | Wed | Contextual bandits          | BA 18         | Ola              |
| Oct 16 | Mon | Linear bandits              | BA 19         | Shahab           |
| Oct 18 | Wed | Bayesian bandits            | BA 34         | Palash           |
| Oct 23 | Mon | Project discussion          | None          | Marek            |
| Oct 25 | Wed | Lower bounds                | BA 13, 15, 16 | Satanu           |
| Oct 30 | Mon | Partial monitoring and MDPs | BA 37, 38     | Noushad          |
| Nov 1  | Wed | Dynamic programming         | MDP 6, RL 6   | Xihong           |
| Nov 6  | Mon | Risk aversion               |               | Monkie           |
| Nov 8  | Wed | Imitation learning and IRL  | Dag           | Ola              |
| Nov 13 | Mon | LQR and MPC                 | MPC           | Shahab           |
| Nov 15 | Wed | Policy gradients (PPO)      | RL13, PPO     | Gersi            |
| Nov 20 | Mon | RLHF                        | LAN           | Satanu           |
| Nov 22 | Wed | Thanksgiving                |               |                  |
| Nov 27 | Mon | Transformers                | TRN           | Palash & Noushad |
| Nov 29 | Wed | Game theory IRL             | GM, 11        | Bryan            |
| Dec 4  | Mon | Exploration for LQR         | LEX           | Saber            |
| Dec 6  | Wed | Project discussion, posters |               | Everyone         |
| Dec 8  | Fri | Poster presentations        |               |                  |


## Textbooks

- **BA**: [Lattimore and Szepesvari (2019), Bandit Algorithms](https://tor-lattimore.com/downloads/book/book.pdf)
- **IMB**: [Aleksandrs Slivkins (2019), Introduction to Multi-armed Bandits](https://arxiv.org/pdf/1904.07272.pdf)
- **CI**: Boucheron, S., Lugosi, G., & Massart, P. (2013). Concentration Inequalities: A Nonasymptotic Theory of Independence. Oxford University Press.

- **MDP**: Puterman, M. L. (2005). Markov decision processes: Discrete stochastic dynamic programming. Wiley-Interscience.
- **RL**: Sutton, R. S., & Barto, A. G. (2018). Reinforcement learning: An introduction (2nd ed.). The MIT Press.

- **GM**: [Francesco Orabona, A Modern Introduction to Online Learning](https://arxiv.org/abs/1912.13213)


## Papers

- **Dag**: Ross, S., Gordon, G., & Bagnell, D. (2011). A reduction of imitation learning and structured prediction to no-regret online learning. International Conference on Artificial Intelligence and Statistics (AISTATS), 627–635.
- **MPC**: [Model Predictive Control: Theory, Computation, and Design](https://sites.engineering.ucsb.edu/~jbraw/mpc/)
- **PPO**: [Schulman, J., Wolski, F., Dhariwal, P., Radford, A., & Klimov, O. (2017). Proximal Policy Optimization Algorithms (arXiv:1707.06347)](http://arxiv.org/abs/1707.06347)
- **LEX**: [Simchowitz, M., & Foster, D. J. (2020). Naive Exploration is Optimal for Online LQR. International Conference on Machine Learning](http://proceedings.mlr.press/v119/simchowitz20a/simchowitz20a.pdf)
- **LAN**: [Many Authors, Training language models to follow instructions
with human feedback, Neurips 2022](https://proceedings.neurips.cc/paper_files/paper/2022/file/b1efde53be364a73914f58805a001731-Paper-Conference.pdf)
- **TRN**: [Mary Phuong, Marcus Hutter, Formal Algorithms for Transformers](https://arxiv.org/abs/2207.09238)

## Office Hours

- *Marek*:  Mon, Wed 2pm - 3pm at Kingsbury N215b

####  Assignments

There will be short theoretical or implementation assignments due for most of the lectures. These assignments will be graded pass/fail. Reasonable effort will earn a pass even when the solution is not correct. Some assignments are likely to be quite challenging. 

We will have a poll to choose which papers to read. Good places to look for papers are: ICML, NeurIPS/NIPS, JMLR, JAIR, Operations Reseach, Math of Operations Research, IEEE TAC, EJOR

## Grading ##

- Topic & paper presentation  (30%) (pass/fail)
- Project  (50%)
- Assignments (20%) (pass/fail)

## Presenting a Textbook Topic ##

Each student is expected to present 3-4 topics, including at least one chapter and one paper. The presentation should aim for clarity and the presenter should be able to answer basic questions about the topic.

## Presenting a Paper ##

Please review the following helpful guide on how to review read research papers. [How to read a research paper](docs/efficientReading.pdf)

When presenting a paper or writing a response to it, you should address the following points:

1. *Motivation*: Why is this problem important?
2. *Challenge*: Why is it difficult?
3. *Approach*: What is the main approach?
4. *Novelty*: What is new about the paper?
5. *Evidence*: Is there sufficient evidence of the effectiveness of the methods?
6. *Limitations*: What is it that the paper does not address? Are there alternative methods that can work?

Please prepare slides for the presentation that include relevant figures or tables from the paper.


## Writing a Paper Response ##

For every paper (column reading says: "paper"), every student is expected to write a short paper response. The response should consist of:

1. A paragraph describing the main idea of the paper
2. 2-4 paragraphs addressing the points mentioned in the section "Presenting a Paper"

## Project ##

The project is a group project with group size of 1 - 5. I will offer several options or you may choose a topic that interests you.
